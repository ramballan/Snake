package ru.epam;

import ru.epam.objects.Animal;
import ru.epam.objects.Frog;
import ru.epam.objects.Snake;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Game Controller
 */
public class Controller extends Thread{
    private static int result;
    private static ArrayList<Frog> frogs = new ArrayList<Frog>();
    private static Snake snake;
    private static boolean startFlag = false;
    //start parametrs
    private static int length,posX,posY,frogNumber, sleepTime,difficulty;
    private static MainFrame frame;

    public Controller(int length, int posX, int posY, int frogNumber, int sleepTime, int difficulty, MainFrame frame){
        Controller.length =length;
        Controller.posX =posX;
        Controller.posY =posY;
        Controller.frogNumber = frogNumber;
        Controller.sleepTime =sleepTime;
        Controller.difficulty =difficulty;
        Controller.frame = frame;
    }

    public static void startGame(){
        result = 0;
        frame.disableStartButton();
        frame.enableStopButton();
        snake = new Snake(sleepTime, length,posX,posY);
        frogs.clear();
        Animal.clearField();
        frame.prepareToStart();
        for (int i = 0; i < frogNumber; i++) {
            //sleep time for frog = snakeSleepTime * difficulty
            frogs.add(new Frog(sleepTime * difficulty));
        }
        startFlag = true;
        snake.start();
        for (Frog frog : frogs) {
            frog.start();
        }
    }
    public static void stopGame(){
        frame.disableStopButton();
        frame.enableStartButton();
        snake.kill();
        for (Frog frog : frogs) {
            frog.kill();
        }
        startFlag = false;
        JOptionPane.showMessageDialog(frame,"Ваш результат: "+result,"",JOptionPane.INFORMATION_MESSAGE,frame.getHypnoFrogImage());
    }
    public static void changeDirectionLeft(){
        if(startFlag){
            snake.changeDirectionLeft();
        }
    }
    public static void changeDirectionRight(){
        if(startFlag) {
            snake.changeDirectionRight();
        }
    }

    @Override
    public void run() {
        while (true){
            synchronized (Animal.getSync()){
                if (startFlag){
                    Iterator<Frog> frogsIter = frogs.iterator();
                    Frog frog;
                    boolean createFlag=false;
                    while(frogsIter.hasNext()){
                        frog = frogsIter.next();
                        frame.drawFrog(frog);
                        //if our snake has eaten this frog
                        if (frog.getX()==snake.getHeadX()&&(frog.getY()==snake.getHeadY())){
                            frog.kill();
                            frogsIter.remove();
                            snake.incLength();
                            frame.setResult(++result);
                            createFlag=true;
                        }
                    }
                    if (createFlag){
                        frog=new Frog(sleepTime * difficulty);
                        frog.start();
                        frogs.add(frog);
                    }
                    frame.drawSnake(snake);
                }
            }
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
