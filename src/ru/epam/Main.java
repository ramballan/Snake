package ru.epam;

import javax.swing.*;

public class Main {
    /**
     * the coefficient for the calculation of normal number of frogs
     * formula for calculation
     * <pre>{@code
     * frogNumber = width*height/K_FROG_NOM
     * }</pre>
     */
    private static final int K_FROG_NOM = 30;
    /**
     * the coefficient for the calculation of maximum number of frogs
     * formula for calculation
     * <pre>{@code
     * maxFrogNumber = width*height/K_FROG_MAX
     * }</pre>
     */
    private static final int K_FROG_MAX = 10;
    /**
     * The coefficient for the calculation of snake sleep time.
     * Formula for calculation:
     * <pre>{@code
     * sleepTime = gameSpeed*K_SLEEP_TIME
     * }</pre>
     */
    private static final int K_SLEEP_TIME = 150;
    public static void main(String[] args) {
        JFrame frame = new JFrame();//this frame only for JDialogs
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        String message = "введите высоту игрового поля";
        int height = 20;
        height = inputData(frame,message,height,6,40);
        message = "введите ширину игрового поля";
        int width = 20;
        width = inputData(frame,message,width,6,40);
        message = "введите начальную длину змеи";
        int lenght = 2;
        lenght = inputData(frame,message,lenght,2,10);
        message = "введите начальную позицию змеи по горизонтали";
        int posX = 1;
        posX = inputData(frame,message,posX,1,width);
        message = "введите начальную позицию змеи по вертикали";
        int posY = 1;
        posY = inputData(frame,message,posY,1,height);
        message = "выберите скорость перемещения от 1(быстро) до 10(медленно)";
        int gameSpeed = 5;
        gameSpeed = inputData(frame,message,gameSpeed,1,10);
        message = "выберите уровень сложности 1-сложный, 2-средний; 3-легкий";
        int difficulty = 3;
        difficulty = inputData(frame,message,difficulty,1,3);
        message = "выберите количество лягушек ";
        //calculation of normal number of frogs
        int frogNumber = width*height/K_FROG_NOM;
        //input frog number with limit check min = 1; max = width*height/K_FROG_MAX
        frogNumber = inputData(frame,message,frogNumber,1,width*height/K_FROG_MAX);

        frame = new MainFrame("Змейка",height,width);
        //calculation of snake sleep time
        int sleepTime = gameSpeed*K_SLEEP_TIME;
        new Controller(lenght,posX-1,posY-1,frogNumber,sleepTime,difficulty, (MainFrame) frame).start();
    }

    /**
     * creates input dialog and retuns integer value
     * @param frame  parent frame
     * @param message  message for the user
     * @param defaultValue  default value for parametr
     * @param minVal minimum value for parametr
     * @param maxVal maximum value for parametr
     * @return integer value from input field
     */
    private static int inputData(JFrame frame, String message, int defaultValue, int minVal, int maxVal){
        while (true){
            try{
                String paramStr = JOptionPane.showInputDialog(frame.getComponent(0),message,defaultValue);
                //press cancel button
                if (paramStr==null){
                    System.exit(2);
                }
                Integer param = Integer.parseInt(paramStr);

                //check parametr minVal<Value<maxVal
                if ((param>=minVal)&&(param<=maxVal)){
                    return param;
                }else{
                    JOptionPane.showMessageDialog(frame.getComponent(0), "Введите число от "+minVal+" до "+maxVal, "ошибка", JOptionPane.ERROR_MESSAGE);
                    continue;
                }
            }catch (NumberFormatException e){
                JOptionPane.showMessageDialog(frame.getComponent(0), "Введите число", "ошибка", JOptionPane.ERROR_MESSAGE);
                e.printStackTrace();
            }

        }
    }
}
