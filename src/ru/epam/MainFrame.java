package ru.epam;
import ru.epam.objects.Animal;
import ru.epam.objects.Frog;
import ru.epam.objects.Snake;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Main frame class
 */
public class MainFrame extends JFrame{
    private static int gameFieldHeight;
    private static int gameFiledWidth;
    private JLabel[][] gameField;
    private ImageIcon emptyImage = new ImageIcon(MainFrame.class.getResource("images/blackSquare.gif"));
    private ImageIcon snakeBodyImage = new ImageIcon(MainFrame.class.getResource("images/yellowBody.gif"));
    private ImageIcon snakeTaleImage = new ImageIcon(MainFrame.class.getResource("images/yellowTale.gif"));
    private ImageIcon snakeHeadImage = new ImageIcon(MainFrame.class.getResource("images/yellowHead.gif"));
    private ImageIcon frogImage = new ImageIcon(MainFrame.class.getResource("images/frog.gif"));
    private ImageIcon hypnoFrogImage = new ImageIcon(MainFrame.class.getResource("images/hypnofrog.gif"));
    private final int IMAGE_HEIGHT = 20;
    private final int IMAGE_WIDTH = 20;
    /**additional constant for frame width*/
    private final int ADD_WIDTH = 120;
    /**additional constant for frame height*/
    private final int ADD_HEIGHT = 40;
    /**The coefficient for the calculation of rows number in right pane*/
    private final int K_RIGHT_PANE = 30;
    private JLabel resultLabel = new JLabel("0");
    private JButton startButton = new JButton("Старт");
    private JButton stopButton = new JButton("Стоп");

    public MainFrame(String frameName, int gameFieldHeight, int gameFiledWidth){
        super(frameName);
        MainFrame.gameFieldHeight = gameFieldHeight;
        MainFrame.gameFiledWidth = gameFiledWidth;
        gameField = new JLabel[gameFieldHeight][gameFiledWidth];
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //frame width = gameFieldWidth*IMAGE_WIDTH + ADD_WIDTH ; frame height = gameFieldHeight*IMAGE_HEIGHT + ADD_HEIGHT
        this.setSize(gameFiledWidth * IMAGE_WIDTH + ADD_WIDTH, gameFieldHeight * IMAGE_HEIGHT + ADD_HEIGHT);
        this.setResizable(false);

        JPanel mainPanel = new JPanel();
        //game Field
        JPanel gridPane = new JPanel();
        //pane with buttons
        JPanel rightPane = new JPanel();
        this.add(mainPanel);
        mainPanel.setLayout(new BorderLayout());
        mainPanel.add(gridPane,BorderLayout.CENTER);
        mainPanel.add(rightPane, BorderLayout.EAST);

        gridPane.setLayout(new GridLayout(gameFieldHeight, gameFiledWidth));
        //number of rows in right pane = (gameFieldHeight * IMAGE_HEIGHT) / K_RIGHT_PANE
        rightPane.setLayout(new GridLayout((gameFieldHeight * IMAGE_HEIGHT) / K_RIGHT_PANE, 1, 5, 5));
        JLabel text = new JLabel("Результат игры");
        text.setHorizontalAlignment(SwingConstants.CENTER);
        resultLabel.setHorizontalAlignment(SwingConstants.CENTER);
        resultLabel.setVerticalAlignment(SwingConstants.TOP);
        resultLabel.setFont(new Font("my font",Font.ROMAN_BASELINE,20));
        stopButton.setEnabled(false);
        //add to screen
        rightPane.add(text);
        rightPane.add(resultLabel);
        rightPane.add(startButton);
        rightPane.add(stopButton);
        for (int x = 0; x < gameField.length; x++) {
            for (int y = 0; y < gameField[x].length; y++) {
                gameField[x][y] = new JLabel(emptyImage);
                gridPane.add(gameField[x][y]);
            }
        }
        this.setVisible(true);
        this.addMouseListener(new MouseAdapter());
        ActionListener actionListener = new ActionListener();
        startButton.addActionListener(actionListener);
        stopButton.addActionListener(actionListener);
    }

    public void prepareToStart(){
        resultLabel.setText("0");
        for (int x = 0; x < gameField.length; x++) {
            for (int y = 0; y < gameField[x].length; y++) {
                gameField[x][y].setIcon(emptyImage);
            }
        }
    }
    /**
     * method draw snake
     * @param snake snake to draw
     */
    public void drawSnake(Snake snake){
        //begin to draw our Snake. delete old tale
        if((snake.getOldTaleX()!=null)&&(!Animal.pointHasAnimal(snake.getOldTaleY(), snake.getOldTaleX()))){
            gameField[snake.getOldTaleY()][snake.getOldTaleX()].setIcon(emptyImage);
            snake.setNullOldTaleX();
            snake.setNullOldTaleY();
        }
        //draw Snake body
        gameField[snake.getBodyY()][snake.getBodyX()].setIcon(snakeBodyImage);
        //draw Snake tale
        gameField[snake.getTaleY()][snake.getTaleX()].setIcon(snakeTaleImage);
        //draw Snake head
        gameField[snake.getHeadY()][snake.getHeadX()].setIcon(snakeHeadImage);
    }
    /**
     * method draw frog
     * @param frog frog to draw
     */
    public void drawFrog(Frog frog){
        if ((frog.getPreviousX()!=null)&&(!Animal.pointHasAnimal(frog.getPreviousY(), frog.getPreviousX()))){
            gameField[frog.getPreviousY()][frog.getPreviousX()].setIcon(emptyImage);
            frog.setNullPreviousY();
            frog.setNullPreviousX();
        }
        gameField[frog.getY()][frog.getX()].setIcon(frogImage);
    }
    public void enableStartButton(){
        startButton.setEnabled(true);
    }
    public void disableStartButton(){
        startButton.setEnabled(false);
    }
    public void enableStopButton(){
        stopButton.setEnabled(true);
    }
    public void disableStopButton(){
        stopButton.setEnabled(false);
    }
    public static int getGameFieldHeight() {
        return gameFieldHeight;
    }
    public static int getGameFieldWidth() {
        return gameFiledWidth;
    }
    public ImageIcon getHypnoFrogImage() {
        return hypnoFrogImage;
    }
    public void setResult(int result) {
        resultLabel.setText(String.valueOf(result));
    }

    /**
     * Mouse Adapter class
     */
    class MouseAdapter extends java.awt.event.MouseAdapter {
        @Override
        public void mousePressed(MouseEvent e) {
            if(e.getButton()==1){   //Left Button Clicked
                Controller.changeDirectionLeft();
            }else if(e.getButton()==3){ //Right Button Clicked
                Controller.changeDirectionRight();
            }
        }
    }

    /**
     * ActionListeners for buttons
     */
    class ActionListener implements java.awt.event.ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource().equals(startButton)) {
                Controller.startGame();
            }
            if (e.getSource().equals(stopButton)) {
                Controller.stopGame();
            }
        }
    }
}
