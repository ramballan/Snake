package ru.epam.objects;

import java.util.HashSet;
import java.util.Random;

public class Frog extends Animal{
    private int x;
    private int y;
    private Integer previousX = null;
    private Integer previousY = null;
    private Random random = new Random();
    private HashSet<Integer> availableMoves = new HashSet<Integer>();

    public Frog(int sleepTime){
        super(sleepTime);
        while (true){
            x = random.nextInt(width);
            y = random.nextInt(height);
            //check place while creating
            if (animals[y][x]==null){
                animals[y][x]=this;
//                System.out.println("I'm frog №"+id+" create at "+x+" "+y);
                break;
            }
        }
    }


    @Override
    void move() {
        //0-up, 1-right, 2-down, 3- left
        availableMoves.clear();
        //check available moves and add them to Hashset
        if ((y-1>=0)&&(animals[y-1][x]==null)) availableMoves.add(0);
        if ((y+1<=height-1)&&(animals[y+1][x]==null)) availableMoves.add(2);
        if ((x-1>=0)&&(animals[y][x-1]==null)) availableMoves.add(3);
        if ((x+1<=width-1)&&(animals[y][x+1]==null)) availableMoves.add(1);
        if (availableMoves.size()>0){
            Integer move;
            while (true){
                move = random.nextInt(4);
                if (availableMoves.contains(move)) break;
            }
            previousX = x;
            previousY = y;
            switch (move){
                case 0: y-=1;
                    break;
                case 1: x+=1;
                    break;
                case 2: y+=1;
                    break;
                case 3: x-=1;
                    break;
            }
            animals[previousY][previousX]=null;
            animals[y][x]=this;
//            System.out.println("I'm frog jump to "+x+" "+y);
        }else{
            previousX = null;
            previousY = null;
        }
    }
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
    public Integer getPreviousX() {
        return previousX;
    }
    public Integer getPreviousY() {
        return previousY;
    }
    public void setNullPreviousX() {
        previousX = null;
    }
    public void setNullPreviousY() {
        previousX = null;
    }
}
