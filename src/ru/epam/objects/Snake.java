package ru.epam.objects;

import ru.epam.Controller;

import java.util.LinkedList;

public class Snake extends Animal {
    private static int length;
    private int direction = 1;
    private int cmd = 0; //-1= left; 1 = right
    private Integer oldTaleX = null;
    private Integer oldTaleY = null;
    private LinkedList<Integer> x = new LinkedList<Integer>();
    private LinkedList<Integer> y = new LinkedList<Integer>();
    private int posX,posY;
    public Snake(int sleepTime, int lenght, int posX, int posY) {
        super(sleepTime);
        length = lenght;
        x.clear();
        y.clear();
        x.add(posX);
        y.add(posY);
        direction=1;
        this.posX = posX;
        this.posY = posY;
    }

    @Override
    void move() {
        direction+=cmd;
        //reset cmd
        cmd=0;
        if (direction==-1) direction=3;
        if (direction==4) direction=0;
        switch (direction){
            //0 - up; 1 - right; 2 - down; 3 - left
            case 0: if (y.getFirst()==0) y.addFirst(height-1);
                else y.addFirst(y.getFirst()-1);
                x.addFirst(x.getFirst());
                break;
            case 1: if (x.getFirst()==width-1) x.addFirst(0);
                else x.addFirst(x.getFirst()+1);
                y.addFirst(y.getFirst());
                break;
            case 2: if (y.getFirst()==height-1) y.addFirst(0);
                else y.addFirst(y.getFirst()+1);
                x.addFirst(x.getFirst());
                break;
            case 3: if (x.getFirst()==0) x.addFirst(width-1);
                else x.addFirst(x.getFirst()-1);
                y.addFirst(y.getFirst());
                break;
        }
        Animal animal = animals[y.getFirst()][x.getFirst()];
        //if in this position Snake animal, and not his tale
        if ((animal!=null)&&(animal.getClass().equals(this.getClass()))&&
                !((y.getFirst().equals(y.getLast()))&&(x.getFirst().equals(x.getLast())))) {
            Controller.stopGame();
            live = false;
        }
        //if our snake doesn't grow
        if ((x.size()>length)&&(y.size()>length)){
            animals[y.getLast()][x.getLast()]=null;
            oldTaleX=x.getLast();
            x.removeLast();
            oldTaleY=y.getLast();
            y.removeLast();
        }
        for (int i = 0; i < x.size(); i++) {
            animals[y.get(i)][x.get(i)]=this;
        }
    }
    public void changeDirectionLeft(){
        cmd = -1;
    }
    public void changeDirectionRight(){
        cmd = 1;
    }
    public void incLength() {
        length++;
    }
    public Integer getOldTaleX() {
        return oldTaleX;
    }
    public void setNullOldTaleX() {
        oldTaleX = null;
    }
    public Integer getOldTaleY() {
        return oldTaleY;
    }
    public void setNullOldTaleY() {
        oldTaleY = null;
    }
    public int getHeadX(){
        return x.getFirst();
    }
    public int getBodyX(){
        if (x.size()>1) return x.get(1);
        else return posX;
    }
    public int getTaleX(){
        return x.getLast();
    }
    public int getHeadY(){
        return y.getFirst();
    }
    public int getBodyY(){
        if (y.size()>1) return y.get(1);
        else return posY;
    }
    public int getTaleY(){
        return y.getLast();
    }
}
