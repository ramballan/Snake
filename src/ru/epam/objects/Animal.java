package ru.epam.objects;

import ru.epam.MainFrame;

/**
 * abstract animal for frogs and snakes
 */
public abstract class Animal extends Thread{
    private static final Object sync = new Object();
    protected volatile static Animal[][] animals = new Animal[MainFrame.getGameFieldHeight()][MainFrame.getGameFieldWidth()];
    int height = MainFrame.getGameFieldHeight();
    int width = MainFrame.getGameFieldWidth();
    protected boolean live = true;
    int sleepTime = 1000;

    public Animal(int sleepTime) {
        this.sleepTime = sleepTime;
    }

    @Override
    public void run() {
        while (live){
            synchronized (sync){
                move();
            }
            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    abstract void move();
    public static void clearField(){
        for (int y = 0; y < animals.length; y++) {
            for (int x = 0; x < animals[y].length; x++) {
                animals[y][x]=null;
            }
        }
    }

    /**
     * check point(x,y) in animals massive if any animal there
     */
    public static boolean pointHasAnimal(int y, int x){
        return animals[y][x] != null;
    }
    /**
     * kill this animal
     */
    public void kill() {
        live = false;
    }
    public static Object getSync() {
        return sync;
    }
}
